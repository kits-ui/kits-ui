import Button from './src/button/index.vue';
import FilterBox from './src/filterBox/index.vue';
import AuthCode from './src/authCode/index.vue';
import Notification from './src/notification/index.vue';
import Image from './src/image/index.vue';
import Watermark from './src/watermark/index.vue';
import Loading from './src/loading';
import ContextMenu from './src/contextMenu/components/Vue3Menus.vue';
import Tooltips from './src/tooltips/index.vue';
import Popover from './src/popover/index.vue';
export default {
  Button,
  FilterBox,
  AuthCode,
  Notification,
  Image,
  Watermark,
  Loading,
  ContextMenu,
  Tooltips,
  Popover,
};
